package Kodutoo;

import java.io.*;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.JOptionPane;

public class writeLog {
    public static void saveUserInput(String text) {
        String filename = "text.txt";
        File output = new File(filename);
        String absolutePath = output.getAbsolutePath();
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy' 'HH:mm:ss");
        Date now = new Date();
        String date = dateFormat.format(now);
        String outputString = "[" + date + "]" + " - " + text; 
        
        try {
            if (output.exists()) {
                BufferedWriter bw = new BufferedWriter(new FileWriter(filename, true));
                bw.newLine();
                bw.write(outputString);
                bw.close();
            } 
            else {
                BufferedWriter bw = new BufferedWriter(new FileWriter(filename));
                bw.write(outputString);
                bw.close();
            }
        } catch (IOException ex) {
            ex.printStackTrace();
        }

        printSavedPath(absolutePath);
    }
    public static void printSavedPath(String path) {
    	JOptionPane.showMessageDialog(null, "Sisestatud andmed salvestatud faili: " + path);
    }
}