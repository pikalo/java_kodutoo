package Kodutoo;

import java.util.Calendar;

public class dataProcess {
	public static String getMonthName(int month) {
		String monthName = "";
		String[] allMonths = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		monthName = allMonths[month - 1];
		return monthName;
	}

	public static int getDaysInMonth(int month, int year) {
		int daysInMonth = 0;
		int[] numOfDays = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

		if ((month == 2) && isLeapYear(year)) {
			numOfDays[month - 1] = 29;
		}

		daysInMonth = numOfDays[month - 1];
		return daysInMonth;
	}

	private static boolean isLeapYear(int year) {
		if (year % 4 == 0) {
			return true;
		} else {
			return false;
		}

	}

	public static int getStartDay(int month, int year) {
		int startDay = 0;
		Calendar calendar = Calendar.getInstance();

		calendar.set(year, month - 1, 0);

		startDay = calendar.get(Calendar.DAY_OF_WEEK);
		return startDay;
	}

	public static int getWeeksInMonth(int startDay, int daysInMonth) {
		int weeksInMonth = 0;
		return weeksInMonth;
	}

}
