package Kodutoo;

import java.awt.*;
import javax.swing.*;

public class Gui {

		public JFrame frame;
		public JTextField[][] Text = new JTextField[7][9];
		int H = 50;
		int W = 80;
		int offx = 10;
		int offy = 10;

		public Gui(int startDay, int daysInMonth, int enteredDay, int enteredMonth, int enteredYear, String monthName,
				String enteredUserName) {
			frame = new JFrame("Kalender");
			frame.setVisible(true);
			frame.setBounds(100, 100, 620, 470);
			frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			frame.getContentPane().setLayout(null);
			frame.getContentPane().setBackground(Color.black);

			String[] allDaysOfWeek = { "Esmaspäev", "Teisipäev", "Kolmapäev", "Neljapäev", "Reede", "Laupäev", "Pühapäev" };
			
			int date = 1;
			for (int i = 0; i < 8; i++) {
				for (int j = 0; j < 7; j++) {

					Text[j][i] = new JTextField();
					if (i == 0) {
						if (j == 0) {
							Text[j][i].setText("Sinu valitud päev - " + monthName + " " + enteredDay + ", " + enteredYear);
							Text[j][i].setBounds(offx + (j * W), offy + (i * H), W * 7, H);
							Text[j][i].setForeground(Color.BLUE);
						} else {
						}
					} else {
						if (i == 1) {
							if ((j == 5) || (j == 6)) {
								Text[j][i].setForeground(Color.black);
								Text[j][i].setBackground(Color.red);
							}
							Text[j][i].setText(allDaysOfWeek[j]);
						} else if ((i == 2) && (j + 1 < startDay)) {

						} else {
							if (date <= daysInMonth) {
								if ((j == 5) || (j == 6)) {
									Text[j][i].setForeground(Color.red);
								}
								if (date == enteredDay) {
									Text[j][i].setText(enteredUserName);
									Text[j][i].setForeground(Color.BLUE);
									Text[j][i].setBackground(Color.CYAN);
								} else {
									Text[j][i].setText(Integer.toString(date));
									Text[j][i].setBackground(Color.white);
								}
								date++;
							}
						}

						Text[j][i].setBounds(offx + (j * W), offy + (i * H), W, H);
					}

					frame.add(Text[j][i]);
				}
			}
			frame.repaint();
		}
}
