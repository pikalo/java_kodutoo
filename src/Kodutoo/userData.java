package Kodutoo;

import javax.swing.JFrame;
import javax.swing.JOptionPane;

public class userData {
	public static int readDay(int limit) {
		int day = 0;
		String numberRegex = "\\d+";
		String userInput = "0";
		
		try {
			do {
				userInput = JOptionPane.showInputDialog("Sisesta päev (1 -" + limit + ")");
			} while (!userInput.matches(numberRegex) || (Integer.parseInt(userInput) == JOptionPane.CANCEL_OPTION));

		} catch (Exception e) {
			System.out.println("StringReadException: " + e.getMessage());
		}
		
		try {
			day = Integer.parseInt(userInput);
		} catch (NumberFormatException nfe) {
			System.out.println("NumberFormatException: " + nfe.getMessage());
		}
		return day;
	}

	public static int readMonth() {
		int month = 0;
		String numberRegex = "\\d+";

		String userInput = null;
		try {
			do {
				userInput = JOptionPane.showInputDialog("Sisesta kuu (1-12)");
			} while (!userInput.matches(numberRegex));

		} catch (Exception e) {
			System.out.println("StringReadException: " + e.getMessage());
		}
		 
		try {
			month = Integer.parseInt(userInput);
		} catch (NumberFormatException nfe) {
			System.out.println("NumberFormatException: " + nfe.getMessage());
		} 
		return month;
	}

	public static int readYear() {
		int year = 0;
		String numberRegex = "\\d+";

		String userInput = null;
		try {
			do {
				userInput = JOptionPane.showInputDialog("Sisesta aasta");

			} while (!userInput.matches(numberRegex));

		} catch (Exception e) {
			System.out.println("StringReadException: " + e.getMessage());
		}
		
		
		try {
			year = Integer.parseInt(userInput);
		} catch (NumberFormatException nfe) {
			System.out.println("NumberFormatException: " + nfe.getMessage());
		}
		return year;
	}

	public static String readName() {
		String name = null;
		String userInput = null;
		String numberRegex = "\\d+";
		try {
			do {
				userInput = JOptionPane.showInputDialog("Sisesta nimi või sündmus");
			} while (userInput.matches(numberRegex));
		} catch (Exception e) {
			System.out.println("ReadNameException: " + e.getMessage());
		}
		name = userInput;
		return name;
		
	}
}