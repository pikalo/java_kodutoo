package Kodutoo;

import Kodutoo.Gui;
import Kodutoo.userData;
import Kodutoo.dataProcess;
import Kodutoo.writeLog;

public class Kalender {

	public static void main(String[] args) {
		new Kalender(0, 0, 0, null);

	}

	public Kalender(int i, int j, int k, String object) {
		initKalender(i, j, k, object);
	}

	private void initKalender(int enteredDay, int enteredMonth, int enteredYear, String enteredName) {
		
		if (enteredMonth == 0) {

			do {
				enteredMonth = userData.readMonth();
			} while ((enteredMonth < 1) || (enteredMonth > 12));
		}

		if (enteredYear == 0) {

			enteredYear = userData.readYear();
		}
		int daysInMonth = dataProcess.getDaysInMonth(enteredMonth, enteredYear);

		if (enteredDay == 0) {

			do {
				enteredDay = userData.readDay(daysInMonth);
			} while ((enteredDay < 1) || (enteredDay > daysInMonth));
		}
		
		if (enteredName == null) {
			
			enteredName = userData.readName();
		}
		
		String enteredMonthName = dataProcess.getMonthName(enteredMonth);

		int startDay = dataProcess.getStartDay(enteredMonth, enteredYear);
		String userString = "Sinu sisestatud andmed: " + enteredName + " " + enteredDay + "-" + enteredMonth + "-"
				+ enteredYear;
		writeLog.saveUserInput(userString);
		Gui test = new Gui(startDay, daysInMonth, enteredDay, enteredMonth, enteredYear, enteredMonthName, enteredName);
	}

}
