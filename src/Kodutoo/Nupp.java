package Kodutoo;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

public class Nupp {

	public static void main(String[] args) throws IOException {
		new Nupp();
	}

	public Nupp() throws IOException {
		initComponents();
	}

	private JFrame viewForm;

	private void initComponents() throws IOException {
		viewForm = new JFrame("Nupp");
		viewForm.setSize(300, 500);
		viewForm.setVisible(true);
		viewForm.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		int H = 25;
		int W = 150;
		int offx = 10;
		int offy = 10;

		JButton button = new JButton("Uus");
		button.setVisible(true);
		button.setLocation(offx, offy);
		button.setSize(W, H);
		viewForm.getContentPane().add(button);
		viewForm.getContentPane().add(new JLabel());
		button.addActionListener(new ActionListener() {

			public void actionPerformed(ActionEvent e) {
				new Kodutoo.Kalender(0, 0, 0, null);
			}
		});

		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		File f = new File("text.txt");
		BufferedReader fin = new BufferedReader(new FileReader(f));
		String line;

		int count = 0;

		while ((line = fin.readLine()) != null) {
			String arrWords[] = line.split(" ");
			String buttonName = arrWords[6] + " " + arrWords[7];
			String name = arrWords[6];
			String dateSplit[] = arrWords[7].split("-");

			count++;

			JButton button1 = new JButton(buttonName);
			button1.setVisible(true);
			button1.setLocation(offx, offy + ((H + offy) * count));
			button1.setSize(W, H);
			viewForm.getContentPane().add(button1);
			viewForm.getContentPane().add(new JLabel());
			button1.addActionListener(new ActionListener() {

				public void actionPerformed(ActionEvent e) {

					new Kodutoo.Kalender(Integer.parseInt(dateSplit[0]), Integer.parseInt(dateSplit[1]),
							Integer.parseInt(dateSplit[2]), name);

				}
			});
		}
	}

}